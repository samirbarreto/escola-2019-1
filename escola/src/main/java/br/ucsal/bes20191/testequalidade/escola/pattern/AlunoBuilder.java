package br.ucsal.bes20191.testequalidade.escola.pattern;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	private Aluno aluno;
	
	public AlunoBuilder() {
		aluno = new Aluno();
	}
	
	public AlunoBuilder comMatricula(Integer metricula) {
		aluno.setMatricula(metricula);

		return this;
	}

	public AlunoBuilder comNome(String nome) {
		aluno.setNome(nome);

		return this;
	}

	public AlunoBuilder emSituacao(SituacaoAluno situacao) {
		aluno.setSituacao(situacao);

		return this;
	}

	public AlunoBuilder nascidoEm(Integer anoNascimento) {
		aluno.setAnoNascimento(anoNascimento);

		return this;
	}
	
	public Aluno obterAluno() {
		return aluno;
	}
}
