package br.ucsal.bes20191.testequalidade.escola.business;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.domain.SituacaoAluno;
import br.ucsal.bes20191.testequalidade.escola.pattern.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;

public class AlunoBOUnitarioTest {
	
	@Mock
	private AlunoDAO alunoDAO;
	
	@Before
    public void setup() {
		alunoDAO = Mockito.mock(AlunoDAO.class);
    }

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		Integer matricula = 1;
		
		Aluno result = buildAlunoMateus();

		Mockito.when(alunoDAO.encontrarPorMatricula(matricula)).thenReturn(result);

	    AlunoBO alunoBO = new AlunoBO(alunoDAO, new DateHelper());
	    
	    int idadeCalculada = alunoBO.calcularIdade(matricula);

	    assertEquals(idadeCalculada, 21);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
    @Test
    public void testarAtualizacaoAlunosAtivos() {
    	Aluno result = buildAlunoSamir();

    	Mockito.when(alunoDAO.encontrarPorMatricula(2)).thenReturn(result);

        Aluno aluno = alunoDAO.encontrarPorMatricula(2);

        AlunoBO alunoBO = new AlunoBO(alunoDAO, new DateHelper());
        alunoBO.atualizar(aluno);

        Mockito.verify(alunoDAO).salvar(aluno);
    }

    private Aluno buildAlunoMateus() {
    	return new AlunoBuilder()
				.comMatricula(1)
				.comNome("Mateus")
				.nascidoEm(1998)
				.emSituacao(SituacaoAluno.CANCELADO)
				.obterAluno();
    }

    private Aluno buildAlunoSamir() {
    	return new AlunoBuilder()
				.comMatricula(2)
				.comNome("Samir")
				.nascidoEm(1999)
				.emSituacao(SituacaoAluno.ATIVO)
				.obterAluno();
    }
}
