package br.ucsal.bes20191.testequalidade.escola.tui;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;


public class TuiHelperUnitarioTest {

	private TuiUtil tuiUtil;


	@Before
	public void setup(){
		tuiUtil = new TuiUtil();
	}

	/**
	 * Verificar a obtenção do nome completo. Caso de teste: primeiro nome
	 * "Claudio" e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("Claudio\nNeiva\n".getBytes());
		System.setIn(inputStream);

		tuiUtil.scanner = new Scanner(System.in);

		String nomeCompleto = tuiUtil.obterNomeCompleto();

		assertEquals(nomeCompleto, "Claudio Neiva");
	}


	/**
	 * Verificar a obten��o exibi��o de mensagem. Caso de teste: mensagem "Tem que estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		tuiUtil.exibirMensagem("Tem que estudar.");

		assertEquals("Bom dia! Tem que estudar.\n", output.toString());
	}
}
